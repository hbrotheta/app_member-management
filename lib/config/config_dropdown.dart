import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';


//todo for 문으로 사용해보자
const List<DropdownMenuItem<String>> dropdownMachineType = [
  DropdownMenuItem(value: 'WASHER', child: Text('세탁기')),
  DropdownMenuItem(value: 'DRYER', child: Text('건조기')),
];

const List<DropdownMenuItem<int>> dropdownYear = [
  DropdownMenuItem(value: 2021, child: Text('2021년')),
  DropdownMenuItem(value: 2022, child: Text('2022년')),
  DropdownMenuItem(value: 2023, child: Text('2023년')),
];

const List<FormBuilderFieldOption<String>> dropdownTeam = [
  FormBuilderFieldOption(value: 'DEV', child: Text('개발팀')),
  FormBuilderFieldOption(value: 'MARKETING', child: Text('마케팅팀')),
  FormBuilderFieldOption(value: 'SALES', child: Text('영업팀')),
];

const List<DropdownMenuItem<String>> dropdownVacationType = [
  DropdownMenuItem(value: 'ANNUAL_LEAVE', child: Text('연차')),
  DropdownMenuItem(value: 'SICK_LEAVE', child: Text('병가')),
  DropdownMenuItem(value: 'ETC', child: Text('기타')),
];
