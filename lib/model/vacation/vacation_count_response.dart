class VacationCountResponse {
  String dateVacationEnd;
  String dateVacationStart;
  String remainCount;
  String vacationCount;
  String vacationTotal;


  VacationCountResponse(
      this.dateVacationEnd,
      this.dateVacationStart,
      this.remainCount,
      this.vacationCount,
      this.vacationTotal,
  );

  factory VacationCountResponse.fromJson(Map<String, dynamic> json) {
    return VacationCountResponse(
      json['dateVacationEnd'],
      json['dateVacationStart'],
      json['remainCount'],
      json['vacationCount'],
      json['vacationTotal'],
    );
  }
}