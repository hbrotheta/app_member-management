import 'package:app_member_management/model/login/login_response.dart';
import 'package:app_member_management/model/vacation/vacation_count_response.dart';

class VacationCountResult {
  VacationCountResponse data;
  bool isSuccess;
  int code;
  String msg;

  VacationCountResult(
      this.data,
      this.isSuccess,
      this.code,
      this.msg
      );

  factory VacationCountResult.fromJson(Map<String, dynamic> json) {
    return VacationCountResult(
        VacationCountResponse.fromJson(json['data']),
        json['isSuccess'],
        json['code'],
        json['msg']
    );
  }
}