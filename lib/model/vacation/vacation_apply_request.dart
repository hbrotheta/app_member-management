class VacationApplyRequest {
  String vacationStart;
  String vacationEnd;
  String vacationType;
  double increaseOrDecreaseValue;
  String vacationReason;

  VacationApplyRequest(
      this.vacationStart,
      this.vacationEnd,
      this.vacationType,
      this.increaseOrDecreaseValue,
      this.vacationReason,
      );
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['vacationStart'] = vacationStart;
    data['vacationEnd'] = vacationEnd;
    data['vacationType'] = vacationType;
    data['increaseOrDecreaseValue'] = increaseOrDecreaseValue;
    data['vacationReason'] = vacationReason;

    return data;

  }


}