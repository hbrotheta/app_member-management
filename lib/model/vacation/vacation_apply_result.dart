import 'package:app_member_management/model/vacation/vacation_apply_response.dart';

class VacationApplyResult {
  List<VacationApplyResponse> list;
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;
  int totalPage;
  int currentPage;

  VacationApplyResult(
      this.list,
      this.isSuccess,
      this.code,
      this.msg,
      this.totalItemCount,
      this.totalPage,
      this.currentPage);

  factory VacationApplyResult.fromJson(Map<String, dynamic> json) {
    return VacationApplyResult(
        json['list'] == null ? [] : (json['list'] as List).map((e) => VacationApplyResponse.fromJson(e)).toList(),
        json['isSuccess'],
        json['code'],
        json['msg'],
        json['totalItemCount'],
        json['totalPage'],
        json['currentPage']
    );
  }
}