class VacationApplyResponse {
  int? vacationTotalUsageId;
  String approvalStatus; //todo null 값으로 들어옴 수정 필요
  String? team;
  String? memberName;
  String? position;
  String? vacationType;
  String? dateApproving;
  num? vacationApplyValue; //체크

  VacationApplyResponse(
      this.vacationTotalUsageId,
      this.approvalStatus,
      this.team,
      this.memberName,
      this.position,
      this.vacationType,
      this.dateApproving,
      this.vacationApplyValue);

  factory VacationApplyResponse.fromJson(Map<String, dynamic> json) {
    return VacationApplyResponse(
      json['vacationTotalUsageId'] ,
      json['approvalStatus'] == null ? '-' : json['approvalStatus'],
      json['team'],
      json['memberName'],
      json['position'],
      json['VacationType'],
      json['dateApproving'],
      json['vacationApplyValue'],
    );
  }
}