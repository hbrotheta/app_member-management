import 'package:app_member_management/model/daily_check/daily_check_response.dart';

class DailyCheckResult {
  DailyCheckResponse data;
  bool isSuccess; // 성공 여부를 묻는 변수
  int code; // isSuccess 결과에 따른 코드 값을 담는 변수
  String msg; // isSuccess 결과에 따른 메세지를 담는 변수

  DailyCheckResult(this.data, this.isSuccess, this.code, this.msg);

  factory DailyCheckResult.fromJson(Map<String, dynamic> json) {
    return DailyCheckResult(
        DailyCheckResponse.fromJson(json['data']),
        json['isSuccess'],
        json['code'],
        json['msg']
    );
  }
}