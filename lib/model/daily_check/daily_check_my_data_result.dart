import 'package:app_member_management/model/daily_check/daily_check_my_data_response.dart';

class DailyCheckMyDataResult {
  List<DailyCheckMyDataResponse> list;
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;
  int totalPage;
  int currentPage;

  DailyCheckMyDataResult(this.list, this.isSuccess, this.code, this.msg, this.totalItemCount, this.totalPage, this.currentPage);

  factory DailyCheckMyDataResult.fromJson(Map<String, dynamic> json) {
    return DailyCheckMyDataResult(
        json['list'] == null ? [] : (json['list'] as List).map((e) => DailyCheckMyDataResponse.fromJson(e)).toList(),
        json['isSuccess'],
        json['code'],
        json['msg'],
        json['totalItemCount'],
        json['totalPage'],
        json['currentPage']
    );
  }
}