class DailyCheckMyDataResponse {
  String? memberName;
  String? dateBase;
  String? dateWorkStart;
  String? dateShortOuting;
  String? dateWorkComeBack;
  String? dateWorkEnd;

  DailyCheckMyDataResponse(
      this.memberName,
      this.dateBase,
      this.dateWorkStart,
      this.dateShortOuting,
      this.dateWorkComeBack,
      this.dateWorkEnd);

  factory DailyCheckMyDataResponse.fromJson(Map<String, dynamic> json) {
    return DailyCheckMyDataResponse(
      json['memberName'],
      json['dateBase'],
      json['dateWorkStart'],
      json['dateShortOuting'],
      json['dateWorkComeBack'],
      json['dateWorkEnd'],
    );
  }
}