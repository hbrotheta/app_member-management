class LoginResponse {
  int memberId;
  String username;
  String dateJoin;

  LoginResponse(this.memberId, this.username, this.dateJoin);

  factory LoginResponse.fromJson(Map<String, dynamic> json) {
    return LoginResponse(
      json['memberId'],
      json['username'],
      json['dateJoin']
    );
  }
}