import 'package:app_member_management/model/member/member_response.dart';

class MemberResult {
  bool isSuccess;
  int code;
  String msg;
  MemberResponse? data;

  MemberResult(this.isSuccess, this.code, this.msg, {this.data});

  factory MemberResult.fromJson(Map<String, dynamic> json) {
    return MemberResult(
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
      data: json['data'] == null
          ? null
          : MemberResponse.fromJson(json['data']),
    );
  }
}