class MemberResponse {
  String profileUrl;
  String username;
  String memberName;
  String team;
  String position;
  String gender;
  String memberPhone;
  String dateJoin;

  MemberResponse(this.profileUrl, this.username, this.memberName, this.team,
      this.position, this.gender, this.memberPhone, this.dateJoin);

  factory MemberResponse.fromJson(Map<String, dynamic> json) {
    return MemberResponse(
      json['profileUrl'],
      json['username'],
      json['memberName'],
      json['team'],
      json['position'],
      json['gender'],
      json['memberPhone'],
      json['dateJoin'],
    );
  }
}