import 'package:app_member_management/pages/page_daily_check_my_data.dart';
import 'package:app_member_management/pages/page_daily_check.dart';
import 'package:app_member_management/pages/page_my_data.dart';
import 'package:app_member_management/pages/page_vacation_apply.dart';
import 'package:app_member_management/pages/page_vacation_my_applies.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  int _currentIndex = 0;
  late PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox.expand(
        child: PageView(
          controller: _pageController,
          onPageChanged: (index) {
            setState(() => _currentIndex = index);
          },
          children: <Widget>[
            const PageMyData(),
            const PageDailyCheck(),
            const PageDailyCheckMyData(),
            const PageVacationApply(),
            const PageVacationMyApplies(),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavyBar(
        selectedIndex: _currentIndex,
        onItemSelected: (index) {
          setState(() => _currentIndex = index);
          _pageController.jumpToPage(index);
        },
        items: <BottomNavyBarItem>[
          BottomNavyBarItem(
              title: Text('마이페이지'),
              icon: Icon(Icons.home)
          ),
          BottomNavyBarItem(
              title: Text('근태 관리'),
              icon: Icon(Icons.login)
          ),
          BottomNavyBarItem(
              title: Text('근태 기록'),
              icon: Icon(Icons.fact_check_rounded)
          ),
          BottomNavyBarItem(
              title: Text('휴가 신청'),
              icon: Icon(Icons.add_box)
          ),
          BottomNavyBarItem(
              title: Text('휴가 관리'),
              icon: Icon(Icons.festival)
          ),
        ],
      ),
    );
  }
}
