import 'package:app_member_management/components/component_appbar_actions.dart';
import 'package:app_member_management/components/component_custom_loading.dart';
import 'package:app_member_management/components/component_notification.dart';
import 'package:app_member_management/config/config_dropdown.dart';
import 'package:app_member_management/config/config_form_validator.dart';
import 'package:app_member_management/model/vacation/vacation_apply_request.dart';
import 'package:app_member_management/repository/repo_vacation.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';
class PageVacationApply extends StatefulWidget {
  const PageVacationApply({Key? key}) : super(key: key);

  @override
  State<PageVacationApply> createState() => _PageVacationRequestState();
}

class _PageVacationRequestState extends State<PageVacationApply> {

  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _setVacationApply(VacationApplyRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoVacation().setVacationApply(request).then((res) {
      BotToast.closeAllLoading(); // 커스텀로딩 닫고

      ComponentNotification(
        success: true,
        title: '휴가 신청 성공.',
        subTitle: res.msg,
      ).call();

      Navigator.pop(
          context,
          [true] // <- 이부분.. 다 처리하고 정상적으로 닫혔다!!
      );
    }).catchError((err) {
      BotToast.closeAllLoading();
//todo 익셉션처리 하자
      ComponentNotification(
        success: false,
        title: '휴가 신청 실패.',
        subTitle: '휴가 신청에 실패 하였습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarActions(
        title: '휴가 신청',
      ),
      body: _buildBody(context)
    );
  }
//todo 컨테이너 위에 휴가 수량 넣기 및 동작 확인
  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(50),
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              children: [
                FormBuilderDateTimePicker(
                  name: 'vacationStart',
                  format: DateFormat('yyyy-MM-dd'),
                  inputType: InputType.date,
                  decoration: InputDecoration(
                    labelText: '휴가 시작일',
                    suffixIcon: IconButton(
                      icon: const Icon(Icons.calendar_month_outlined),
                      onPressed: () {
                        _formKey.currentState!.fields['vacationStart']
                            ?.didChange(null);
                      },
                    ),
                  ),
                  locale: const Locale.fromSubtags(languageCode: 'ko'),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(errorText: formErrorRequired)
                  ]),
                ), // 휴가 시작일
                FormBuilderDateTimePicker(
                  name: 'vacationEnd',
                  format: DateFormat('yyyy-MM-dd'),
                  inputType: InputType.date,
                  decoration: InputDecoration(
                    labelText: '휴가 마지막일',
                    suffixIcon: IconButton(
                      icon: const Icon(Icons.calendar_month_outlined),
                      onPressed: () {
                        _formKey.currentState!.fields['vacationEnd']
                            ?.didChange(null);
                      },
                    ),
                  ),
                  locale: const Locale.fromSubtags(languageCode: 'ko'),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(errorText: formErrorRequired)
                  ]),
                ),
                FormBuilderDropdown<String>(
                  name: 'vacationType',
                  decoration: const InputDecoration(
                    labelText: '휴가 신청 선택',
                  ),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(errorText: formErrorRequired)
                  ]),
                  items: dropdownVacationType,
                ),
                FormBuilderTextField(
                  name: 'increaseOrDecreaseValue',
                  decoration: const InputDecoration(
                    labelText: '연차 수량',
                  ),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(errorText: formErrorRequired),
                  ]),
                  keyboardType: TextInputType.number,
                ),
                FormBuilderTextField(
                  name: 'vacationReason',
                  decoration: const InputDecoration(
                    labelText: '신청 사유',
                  ),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(errorText: formErrorRequired),
                    FormBuilderValidators.minLength(2, errorText: formErrorMinLength(2)),
                    FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(15)),
                  ]),
                  keyboardType: TextInputType.text,
                ), //휴가 사유
                 // 휴가 마지막일
                SizedBox(height: 50),
                OutlinedButton(
                  onPressed: () {
                    if (_formKey.currentState?.saveAndValidate() ?? false) {
                      VacationApplyRequest request = VacationApplyRequest(
                        DateFormat('yyyy-MM-dd').format(_formKey.currentState!.fields['vacationStart']!.value),
                        DateFormat('yyyy-MM-dd').format(_formKey.currentState!.fields['vacationEnd']!.value),
                        _formKey.currentState!.fields['vacationType']!.value,
                        _formKey.currentState!.fields['increaseOrDecreaseValue']!.value,
                        _formKey.currentState!.fields['vacationReason']!.value,

                      );
                      _setVacationApply(request);
                    }
                  },
                  child: Text('등록'),
                ),// 휴가 마지막일
              ],
            ),
          ),
        )
    );
  }
}
