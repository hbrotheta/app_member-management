import 'package:app_member_management/components/component_appbar_actions.dart';
import 'package:app_member_management/components/component_custom_loading.dart';
import 'package:app_member_management/repository/repo_daily_check.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class PageDailyCheck extends StatefulWidget {
  const PageDailyCheck({Key? key}) : super(key: key);

  @override
  State<PageDailyCheck> createState() => _PageDailyCheckState();
}

class _PageDailyCheckState extends State<PageDailyCheck> {
  String _dailyCheckStateName = '';
  String _dailyCheckState = '';

  @override
  void initState() {
    super.initState();
    _getData();
  }

  Future<void> _getData() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoDailyCheck().getData().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _dailyCheckStateName = res.data.dailyCheckStateName;
        _dailyCheckState = res.data.dailyCheckState;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      print(err);
    });
  }

  Future<void> _putData(String dailyCheckState) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoDailyCheck().putState(dailyCheckState).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _dailyCheckStateName = res.data.dailyCheckStateName;
        _dailyCheckState = res.data.dailyCheckState;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      print(err);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarActions(
        title: '근태 관리',
      ),
      body: _buildBody(),
    );
  }
//todo 버튼 컴포넌트로 만들고 활성 비활성화로 만들어보자 시간이 된다면...
  Widget _buildBody() {
    return SingleChildScrollView(
      child: Column(
        children: [
          Text('현재 상태 : $_dailyCheckStateName'),
          if (_dailyCheckState == 'UNKNOWN')
            OutlinedButton(
                onPressed: () {
                  _putData('WORK_START');
                },
                child: Text('출근')
            ),
          if (_dailyCheckState == 'WORK_START')
            OutlinedButton(
                onPressed: () {
                  _putData('SHORT_OUTING');
                },
                child: Text('외출')
            ),
          if (_dailyCheckState == 'SHORT_OUTING')
            OutlinedButton(
                onPressed: () {
                  _putData('COMEBACK');
                },
                child: Text('복귀')
            ),
          if (_dailyCheckState == 'COMEBACK')
            OutlinedButton(
                onPressed: () {
                  _putData('WORK_END');
                },
                child: Text('퇴근')
            ),
          if (_dailyCheckState == 'WORK_START' || _dailyCheckState == 'SHORT_OUTING')
            OutlinedButton(
                onPressed: () {
                  _putData('WORK_END');
                },
                child: Text('퇴근')
            ),
          if (_dailyCheckState == 'WORK_END' || _dailyCheckState == 'WORK_END')
            Text('퇴근 후에는\n상태를 변경 할 수\n없습니다.'),
        ],
      ),
    );
  }
}