import 'package:app_member_management/components/component_appbar_actions.dart';
import 'package:app_member_management/components/component_custom_loading.dart';
import 'package:app_member_management/components/component_daily_check_data_table.dart';
import 'package:app_member_management/components/component_no_contents.dart';
import 'package:app_member_management/components/component_notification.dart';
import 'package:app_member_management/components/componnet_daily_check_count.dart';
import 'package:app_member_management/config/config_dropdown.dart';
import 'package:app_member_management/model/daily_check/daily_check_my_data_response.dart';
import 'package:app_member_management/repository/repo_daily_check.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class PageDailyCheckMyData extends StatefulWidget {
  const PageDailyCheckMyData({Key? key}) : super(key: key);

  @override
  State<PageDailyCheckMyData> createState() => _PageDailyCheckMyDataState();
}

class _PageDailyCheckMyDataState extends State<PageDailyCheckMyData> {

  List<DailyCheckMyDataResponse> _myList = [];
  int _totalItemCount = 0;

  int _selectedYear = DateTime.now().year;
  String _selectedTeam = 'SALES';
  // 팝업처리 할 때에는 부모창에서 model을 넘겨주고
  // _selectedTeam = widget.memberTeam; 이런식으로 변수 만들면서 바로 값 넣어주면 된다.

  @override
  void initState() {
    super.initState();
    _getDailyCheckMyData();
  }

  Future<void> _getDailyCheckMyData() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoDailyCheck().getDailyCheckMyData().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _myList = res.list;
        _totalItemCount = res.totalItemCount;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarActions(
        title: '폼 요소 변경 예제',
      ),
      body: ListView(
        children: [
          _buildBody(),
          _buildBodyList()
        ],
      ),
    );
  }

  Widget _buildBody() {
    return Container(
      child: Column(
        children: [
          FormBuilderDropdown<int>(
            name: 'year',
            decoration: const InputDecoration(
              labelText: '년도',
            ),
            items: dropdownYear,
            initialValue: _selectedYear,
            onChanged: (val) {
              setState(() {
                _selectedYear = val!;
              });
              // _loadItems or _loadData 메서드 호출!!
              // = 다시 불러와
            },
          ),
          Text('현재 선택된 년도 : $_selectedYear'),
          const Divider(),
          FormBuilderRadioGroup<String>(
            name: 'memberTeam',
            decoration: const InputDecoration(
              labelText: '부서',
            ),
            initialValue: _selectedTeam,
            onChanged: (val) {
              setState(() {
                _selectedTeam = val!;
              });
            },
            options: dropdownTeam,
            controlAffinity: ControlAffinity.trailing,
          ),
          Text('현재 선택된 부서 : $_selectedTeam'),
          const SizedBox(
            height: 20,
          ),
          ComponentDailyCheckCount(
              workCount: _totalItemCount,
              callback: () {},
          ),
        ],
      ),
    );
  }

  Widget _buildBodyList() {
    if (_totalItemCount > 0) {
      return Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _myList.length,
              itemBuilder: (_, index) => ComponentDailyCheckDataTable(
                dailyCheckMyDataResponse: _myList[index],
                callback: () async {},
              ),
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
        // 앱바 높이가 40으로 고정되었으므로 정확한 수치 40을 이제 알 수 있음.
        height: MediaQuery.of(context).size.height - 40 - 20,
        child: const ComponentNoContents(icon: Icons.no_sim_outlined, msg: '데이터가 없습니다.'),
      );
    }
  }
}
