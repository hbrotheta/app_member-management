import 'package:app_member_management/components/component_appbar_popup.dart';
import 'package:app_member_management/components/component_custom_loading.dart';
import 'package:app_member_management/components/component_notification.dart';
import 'package:app_member_management/config/config_form_validator.dart';
import 'package:app_member_management/functions/token_lib.dart';
import 'package:app_member_management/middleware/middleware_login_check.dart';
import 'package:app_member_management/model/login/login_request.dart';
import 'package:app_member_management/repository/repo_member.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageLogin extends StatefulWidget {
  const PageLogin({Key? key}) : super(key: key);

  @override
  State<PageLogin> createState() => _PageLoginState();
}

class _PageLoginState extends State<PageLogin> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _doLogin(LoginRequest loginRequest) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().doLogin(loginRequest).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '로그인 성공',
        subTitle: '로그인에 성공하였습니다.',
      ).call();

      // TODO api에서 받아온 결과값을 token에 넣는다. 여기서 가져왔던 memberId 메모리에 저장 핵심!!!
      TokenLib.setToken(res.data.memberId.toString());

      // 미들웨어에게 부탁해서 토큰값 여부 검사 후 페이지 이동을 부탁한다.
      MiddlewareLoginCheck().check(context);

    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '로그인 실패',
        subTitle: '아이디 혹은 비밀번호를 확인해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(
        title: '로그인',
      ),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: FormBuilder(
        key: _formKey,
        autovalidateMode: AutovalidateMode.disabled,
        child: Column(
          children: [
            FormBuilderTextField(
              name: 'username',
              decoration: const InputDecoration(
                labelText: '아이디',
              ),
              validator: FormBuilderValidators.compose([
                FormBuilderValidators.required(errorText: formErrorRequired),
                FormBuilderValidators.minLength(3, errorText: formErrorMinLength(3)),
                FormBuilderValidators.maxLength(15, errorText: formErrorMaxLength(15)),
              ]),
              keyboardType: TextInputType.text,
            ),
            FormBuilderTextField(
              name: 'password',
              decoration: const InputDecoration(
                labelText: '비밀번호',
              ),
              obscureText: true,
              validator: FormBuilderValidators.compose([
                FormBuilderValidators.required(errorText: formErrorRequired),
                FormBuilderValidators.minLength(3, errorText: formErrorMinLength(3)),
                FormBuilderValidators.maxLength(15, errorText: formErrorMaxLength(15)),
              ]),

              keyboardType: TextInputType.text,
            ),
            OutlinedButton(
                onPressed: () {
                  if (_formKey.currentState?.saveAndValidate() ?? false) {
                    LoginRequest loginRequest = LoginRequest(
                      _formKey.currentState!.fields['username']!.value,
                      _formKey.currentState!.fields['password']!.value,
                    );
                    _doLogin(loginRequest);
                  }
                },
                child: const Text('로그인')
            ),
          ],
        ),
      ),
    );
  }
}
