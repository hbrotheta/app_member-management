import 'package:app_member_management/components/component_appbar_actions.dart';
import 'package:app_member_management/components/component_custom_loading.dart';
import 'package:app_member_management/components/component_no_contents.dart';
import 'package:app_member_management/components/component_notification.dart';
import 'package:app_member_management/components/component_vacation_item.dart';
import 'package:app_member_management/model/vacation/vacation_apply_response.dart';
import 'package:app_member_management/repository/repo_vacation.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class PageVacationMyApplies extends StatefulWidget {
  const PageVacationMyApplies({Key? key}) : super(key: key);

  @override
  State<PageVacationMyApplies> createState() => _PageVacationMyAppliesState();
}

class _PageVacationMyAppliesState extends State<PageVacationMyApplies> {

  List<VacationApplyResponse> _myList = [];
  int _totalItemCount = 0;


  @override
  void initState() {
    super.initState();
    _getVacationMyApplies();
  }

  Future<void> _getVacationMyApplies() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoVacation().getVacationMyApplies().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _myList = res.list;
        _totalItemCount = res.totalItemCount;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();
    });
  }

  //todo 디자인 밑 연차 수량 컨테이너 만들기
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarActions(
        title: '나의 휴가 신청 목록',
      ),
      body: ListView(
        children: [
          _buildBodyList()
        ],
      ),
    );
  }


  Widget _buildBodyList() {
    if (_totalItemCount > 0) {
      return Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _myList.length,
              itemBuilder: (_, index) => ComponentVacationItem(
                vacationApplyResponse: _myList[index],
                callback: () async {},
              ),
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
        // 앱바 높이가 40으로 고정되었으므로 정확한 수치 40을 이제 알 수 있음.
        height: MediaQuery.of(context).size.height - 40 - 20,
        child: const ComponentNoContents(icon: Icons.no_sim_outlined, msg: '데이터가 없습니다.'),
      );
    }
  }

}
