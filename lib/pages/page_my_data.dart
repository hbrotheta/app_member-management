import 'package:app_member_management/components/component_appbar_actions.dart';
import 'package:app_member_management/components/component_custom_loading.dart';
import 'package:app_member_management/components/component_notification.dart';
import 'package:app_member_management/functions/token_lib.dart';
import 'package:app_member_management/repository/repo_member.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

import '../components/component_info_container.dart';

class PageMyData extends StatefulWidget {
  const PageMyData({Key? key}) : super(key: key);

  @override
  State<PageMyData> createState() => _PageMyDataState();
}

class _PageMyDataState extends State<PageMyData> {

  String _profileUrl = '';
  String _username  = '';
  String _memberName  = '';
  String _team  = '';
  String _position  = '';
  String _gender  = '';
  String _memberPhone  = '';
  String _dateJoin  = '';

  @override
  void initState() {
    super.initState();
    _getMyData();
  }


  Future<void> _getMyData() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().getMyData().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _profileUrl = res.data!.profileUrl;
        _username = res.data!.username;
        _memberName = res.data!.memberName;
        _team = res.data!.team;
        _position = res.data!.position;
        _gender = res.data!.gender;
        _memberPhone = res.data!.memberPhone;
        _dateJoin = res.data!.dateJoin;
      });

    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '마이페이지 로딩 실패하였습니다.\n관리자에게 문의하세요.',
      ).call();
    });
  }

  Future<void> _logout(BuildContext context) async {

    // functions/token_lib.dart 에서 만든 로그아웃 기능 호출
    TokenLib.logout(context);
  }

// todo 앱바 어디서 쓸건지
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: const ComponentAppbarActions(
        title: '회원용 메인',
      ),
      body: _buildBody(context),
    );
  }

  //todo 디자인 및 이미지
  Widget _buildBody(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(3),
      width: 700,
      color: Colors.white54,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            height: 10,
          ),
          ComponentInfoContainer(memberResponseData: _profileUrl, listName: '직 급'),
          ComponentInfoContainer(memberResponseData: _username, listName: '아이디'),
          ComponentInfoContainer(memberResponseData: _memberName, listName: '이 름'),
          ComponentInfoContainer(memberResponseData: _team, listName: '부 서'),
          ComponentInfoContainer(memberResponseData: _position, listName: '직 급'),
          ComponentInfoContainer(memberResponseData: _gender, listName: '성 별'),
          ComponentInfoContainer(memberResponseData: _memberPhone, listName: '연락처'),
          ComponentInfoContainer(memberResponseData: _dateJoin, listName: '입사일'),
          SizedBox(
            height: 30,
          ),
          OutlinedButton(
              onPressed: () {
                _logout(context);
              },
              child: const Text('로그아웃')
          ),
        ],
      ),
    );
  }
}
