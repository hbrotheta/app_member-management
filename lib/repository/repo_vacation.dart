import 'package:app_member_management/config/config_api.dart';
import 'package:app_member_management/functions/token_lib.dart';
import 'package:app_member_management/model/common_result.dart';
import 'package:app_member_management/model/vacation/vacation_apply_request.dart';
import 'package:app_member_management/model/vacation/vacation_apply_result.dart';
import 'package:dio/dio.dart';

class RepoVacation {
  //휴가 신청 하기
  Future<CommonResult> setVacationApply(VacationApplyRequest vacationApplyRequest) async {
    const String baseUrl = '$apiUri/vacation/my-apply/{memberId}';

    Dio dio = Dio();
    String? token = await TokenLib.getToken();

    final response = await dio.post(
        baseUrl.replaceAll('{memberId}', token!),
        data: vacationApplyRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  // 연차 신청 내역 가져오기
  Future<VacationApplyResult> getVacationMyApplies() async {
    const String baseUrl = '$apiUri/vacation/my-apply-list/{memberId}';

    Dio dio = Dio();
    String? token = await TokenLib.getToken();

    final response = await dio.get(
        baseUrl.replaceAll('{memberId}', token!),
        options: Options(
            followRedirects: false
        )
    );

    return VacationApplyResult.fromJson(response.data);
  }
  //todo 디테일 아이템 가져오자

  // 연차 수량 내역 가져오기
  Future<VacationApplyResult> getVacationMyCount() async {
    const String baseUrl = '$apiUri/vacation/my-count/{memberId}';

    Dio dio = Dio();
    String? token = await TokenLib.getToken();

    final response = await dio.get(
        baseUrl.replaceAll('{memberId}', token!),
        options: Options(
            followRedirects: false
        )
    );

    return VacationApplyResult.fromJson(response.data);
  }
}