import 'package:app_member_management/config/config_api.dart';
import 'package:app_member_management/functions/token_lib.dart';
import 'package:app_member_management/model/login/login_request.dart';
import 'package:app_member_management/model/login/login_result.dart';
import 'package:app_member_management/model/member/member_result.dart';
import 'package:dio/dio.dart';

class RepoMember {
  Future<LoginResult> doLogin(LoginRequest loginRequest) async {
    const String baseUrl = '$apiUri/login-member/default-data';

    Dio dio = Dio();

    final response = await dio.post(
        baseUrl,
        data: loginRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return LoginResult.fromJson(response.data);
  }

  Future<MemberResult> getMyData() async {
    const String baseUrl = '$apiUri/member-info/{memberId}';

    Dio dio = Dio();

    String? token = await TokenLib.getToken();

    final response = await dio.get(
        baseUrl.replaceAll('{memberId}', token!),
        options: Options(
            followRedirects: false
        )
    );

    return MemberResult.fromJson(response.data);
  }
}