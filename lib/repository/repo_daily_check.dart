import 'package:app_member_management/config/config_api.dart';
import 'package:app_member_management/functions/token_lib.dart';
import 'package:app_member_management/model/daily_check/daily_check_my_data_result.dart';
import 'package:app_member_management/model/daily_check/daily_check_result.dart';
import 'package:dio/dio.dart';

class RepoDailyCheck {
  // 현재 출/외/복/퇴근 상태 가져오기
  Future<DailyCheckResult> getData() async {
    const String baseUrl = '$apiUri/daily-check/state/member-id/{memberId}';

    Dio dio = Dio();
    String? token = await TokenLib.getToken();

    final response = await dio.get(
        baseUrl.replaceAll('{memberId}', token!),
        options: Options(
            followRedirects: false
        )
    );

    return DailyCheckResult.fromJson(response.data);
  }
  // 출/외/복/퇴근 상태 수정하기
  Future<DailyCheckResult> putState(String dailyCheckState) async {
    const String baseUrl = '$apiUri/daily-check/state/{dailyCheckState}/member-id/{memberId}';

    Dio dio = Dio();
    String? token = await TokenLib.getToken();

    final response = await dio.put(
        baseUrl.replaceAll('{dailyCheckState}', dailyCheckState).replaceAll('{memberId}', token!),
        options: Options(
            followRedirects: false
        )
    );

    return DailyCheckResult.fromJson(response.data);
  }
  // 나의 출퇴근 리스트 가져오기
  Future<DailyCheckMyDataResult> getDailyCheckMyData() async {
    const String baseUrl = '$apiUri/daily-check/my-data/{memberId}';

    Dio dio = Dio();
    String? token = await TokenLib.getToken();

    final response = await dio.get(
        baseUrl.replaceAll('{memberId}', token!),
        options: Options(
            followRedirects: false
        )
    );

    return DailyCheckMyDataResult.fromJson(response.data);
  }
}