import 'package:easy_container/easy_container.dart';
import 'package:flutter/material.dart';

class ComponentContainerTest extends StatelessWidget {
  const ComponentContainerTest({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return EasyContainer(
      height: 300,
      width: 300,
      padding: 20,
      elevation: 10,
      onTap: () => debugPrint("Container Tapped"),
      margin: 20,
      borderRadius: 20,
      color: Colors.red,
      child: const CircularProgressIndicator.adaptive(),
    );
  }
}
