import 'package:clay_containers/widgets/clay_container.dart';
import 'package:clay_containers/widgets/clay_text.dart';
import 'package:flutter/material.dart';

class ComponentInfoContainer extends StatelessWidget {

  const ComponentInfoContainer({super.key, required this.listName, required this.memberResponseData});
    final String memberResponseData;
    final String listName;

  @override
  Widget build(BuildContext context) {
    return ClayContainer(
      customBorderRadius: BorderRadius.circular(10),
      width: 200,
      color: Color(0xFFf2f2f2),
      child: Padding(
        padding: EdgeInsets.all(20),
        child: ClayText('$listName : $memberResponseData', emboss: true, size: 15),
      ),
    );
  }
}
