import 'package:app_member_management/components/component_apply_status_box.dart';
import 'package:app_member_management/model/vacation/vacation_apply_response.dart';
import 'package:flutter/material.dart';

class ComponentVacationItem extends StatelessWidget {
  const ComponentVacationItem({super.key, required this.vacationApplyResponse, required this.callback});

  final VacationApplyResponse vacationApplyResponse;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        height: 70,
        width: 200,
        child: Column(
          children: [
            Text('${vacationApplyResponse.vacationType} 신청의 건'),
            Row(
              children: [
                Text('${vacationApplyResponse.dateApproving} ${vacationApplyResponse.vacationType} (${vacationApplyResponse.vacationApplyValue}일)'),
                if (vacationApplyResponse.approvalStatus == '승인')
                  ComponentApplyStatusBox(applyStatus: vacationApplyResponse.approvalStatus, boxColor: Colors.blueAccent, callback: () {})
                else if (vacationApplyResponse.approvalStatus == '대기중')
                  ComponentApplyStatusBox(applyStatus: vacationApplyResponse.approvalStatus, boxColor: Colors.green, callback: () {})
                else if (vacationApplyResponse.approvalStatus == '반려')
                  ComponentApplyStatusBox(applyStatus: vacationApplyResponse.approvalStatus, boxColor: Colors.red, callback: () {})
              ],
            ),
          ],
        ),
      ),
    );
  }
}
