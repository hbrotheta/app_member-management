import 'package:app_member_management/model/daily_check/daily_check_my_data_response.dart';
import 'package:flutter/material.dart';

class ComponentDailyCheckDataTable extends StatelessWidget {
  const ComponentDailyCheckDataTable({super.key,
    required this.dailyCheckMyDataResponse,
    required this.callback
  });

  final DailyCheckMyDataResponse dailyCheckMyDataResponse;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return DataTable(
      columns: const <DataColumn>[
        DataColumn(
          label: Expanded(
            child: Text(
              '일자',
              style: TextStyle(fontStyle: FontStyle.italic),
            ),
          ),
        ),
        DataColumn(
          label: Expanded(
            child: Text(
              '출근시간',
              style: TextStyle(fontStyle: FontStyle.italic),
            ),
          ),
        ),
        DataColumn(
          label: Expanded(
            child: Text(
              '외출시간',
              style: TextStyle(fontStyle: FontStyle.italic),
            ),
          ),
        ),
        DataColumn(
          label: Expanded(
            child: Text(
              '복귀시간',
              style: TextStyle(fontStyle: FontStyle.italic),
            ),
          ),
        ),
        DataColumn(
          label: Expanded(
            child: Text(
              '퇴근시간',
              style: TextStyle(fontStyle: FontStyle.italic),
            ),
          ),
        ),
      ],

      rows: [
        DataRow(cells:[
          DataCell(Text('${dailyCheckMyDataResponse.dateBase}')),
          DataCell(Text('${dailyCheckMyDataResponse.dateWorkStart}')),
          DataCell(Text('${dailyCheckMyDataResponse.dateShortOuting}')),
          DataCell(Text('${dailyCheckMyDataResponse.dateWorkComeBack}')),
          DataCell(Text('${dailyCheckMyDataResponse.dateWorkEnd}')),
        ]),
      ]
    );
  }
}
