import 'package:flutter/material.dart';

class ComponentDailyCheckCount extends StatelessWidget {
  const ComponentDailyCheckCount({
    super.key,
    required this.workCount,
    required this.callback
  });

  final int workCount;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: 30.0,
            width: 100.0,
            decoration: BoxDecoration(color: Colors.blue, borderRadius: BorderRadius.circular(10.0)),
            child: Text('근무일 수 : ${workCount.toString()} 일'),
          ),
        ],
      ),
    );
  }
}
