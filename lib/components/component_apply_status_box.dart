import 'package:flutter/material.dart';

class ComponentApplyStatusBox extends StatelessWidget {
  const ComponentApplyStatusBox({
    super.key,
    required this.applyStatus,
    required this.boxColor,
    required this.callback
  });

  final String applyStatus;
  final Color boxColor;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30.0,
      width: 50.0,
      decoration: BoxDecoration(
        color: boxColor, borderRadius: BorderRadius.circular(10.0)),
      child: Text(applyStatus),
    );
  }
}